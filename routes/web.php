<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/masinos', 'CarsController@index')->name('masinos.index');

//Route::get('/cars/{id}', "CarsController@show")->name('cars.show');

Route::get('/masinos/{id}', 'OwnersController@index')->name('owners.index');

Route::get('/savininkai{id}', 'CarsController@show')->name('owners.show');

Route::group(['middleware' => "auth"], function() {
    Route::get('/masinos/create', 'CarsController@create')->name('masinos.create');
    Route::get('/masinos/{id}/edit', 'CarsController@edit')->name('masinos.edit');
    Route::post('/masinos/store', 'CarsController@store')->name('masinos.store');
    Route::post('/masinos/{id}/update', 'CarsController@update')->name('masinos.update');
    Route::post('/masinos/{id}/destroy', 'CarsController@destroy')->name('masinos.destroy');
});

// GERAS SITAS Route::get('/owners', 'OwnersController@index')->name('owners.index');
//Route::get('/masinos{id}', 'CarsController@destroy')->name('masinos.index');
