<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Car;
use\App\Owner;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::all();

        $carsCount = Car::count(); //grazins skaiciu

        return view("cars", ["cars" => $cars, "carsCount"=>$carsCount]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCars = Car::all();

        return view('createcar', ["allCars" => $allCars]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
        $messages = [
        'reg_number.required' => 'Registracijos numeris jau egzistuoja'
    ];

        $validateData = $request->validate([
        'reg_number'=> 'required|unique:cars|min:6',
    ], $messages);

        $masina = new Car;
        $masina->reg_number = $request->reg_number;
        $masina->brand = $request->brand;
        $masina->model = $request->model;

        //$car->brand = $request->

        $masina->save();

        //Pranesimas apie sekminga pridejima
        Session::flash( 'status', 'Mašina sėkmingai pridėta' );

        return redirect()->route('masinos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //kreipiames i modeli Car


        //susikurti ownercontroller kuriame visi owneriai, paspaudus matytus visos masinos
        $carItem = Car::find($id);

        return view('owners', ["carItem" => $carItem]);

       //$ownerItem = Owner::where("car_id", $id)->get();//sita istrint
        //owners vietoj carItem, keiciu blade pavadinima
       
       // return view('carItem', ["carItem" => $carItem]);
        //, "savininkas"=>$ownerItem ]);

        /*
        $carItem = Car::find($id);
        return view('carItem', ["carItem"=> $carItem]);
        */

        //$ownerItem =Owner::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::find($id);
        

        return view('editcar', ["car" =>$car]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    $messages = [
    'reg_number.required' => 'Registracijos numeris jau egzistuoja'
    ];

    //Validator::make($request->all(), [
    $validateData = $request->validate([
        'reg_number'=> 'required|unique:cars|min:6',
       // 'reg_number'=> 'required|min:6',
    ], $messages);

        $car = Car::find($id);
        $car->reg_number = $request->reg_number;
        $car->brand = $request->brand;
        $car->model = $request->model;

        $car->save();

        return redirect()->route('masinos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $car = Car::find($id);
        $car -> delete();

        Session::flash('status', 'Mašina sėkmingai ištrinta' );

        return redirect()->route('masinos.index');

        //$ownerItem = Owner::where("id", $id)->delete();
    }
}
