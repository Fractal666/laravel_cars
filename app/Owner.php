<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $table = "owners";

     public function car() {
        return $this->hasOne('App\Car', 'id', 'car_id');
    }
}
