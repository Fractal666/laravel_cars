@extends("layouts.app")

@section('content')
<div class="container">

<div class="col-sm-4">
<h1>Nauja mašina</h1>

@if ($errors->any() )
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach

        </ul>
    </div>
@endif

<form method="POST" action="{{ route('masinos.store') }}">
    {{ csrf_field() }}

    Registracijos numeris:
    <input name="reg_number"
    required class="form-control">
    </input>
    Markė:
     <input name="brand"
     value="{{ old('brand') }}"
    required class="form-control">
    </input>
    Modelis:
     <input name="model"
     value="{{ old('model') }}"
    required class="form-control">
    </input>

    <hr>

    <input type="submit" class="btn btn-info" value="Pridėti mašiną">

</form>
</div>
<a href="{{ route('masinos.index') }}">Grizti</a>
</div>

@endsection