@extends("layouts.app")

@section('content')
<div class="container">

<div class="col-sm-4">
<h1>Redaguojama mašina {{ $car->id }}</h1>

@if ($errors->any() )
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach

        </ul>
    </div>
@endif

<form method="POST" action="{{ route('masinos.update', $car->id ) }}">
    {{ csrf_field() }}

     Registracijos numeris:
    <input name="reg_number"
    required class="form-control" value="{{ $car->reg_number }}">
    </input>
    Markė:
     <input name="brand"
    required class="form-control" value="{{ $car->brand }}">
    </input>
    Modelis:
     <input name="model"
    required class="form-control" value="{{ $car->model }}">
    </input>
    <hr>
    <input type="submit" class="btn btn-info" value="Redaguoti mašiną">
</form>
</div>
<a href="{{ route('masinos.index') }}">Grizti</a>
</div>


@endsection