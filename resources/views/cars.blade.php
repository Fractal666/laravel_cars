@extends("layouts.app")

@section('content')
<div class="container">
  <div class="col-md-12">
        @if (Session::has('status'))
            <div class="alert alert-info">{{ Session::get('status') }}</div>
        @endif
    </div>


    <div class="panel-heading">
    <h3 class="panel-title">Mašinos</h3>
  </div>


   <table class="table table-hover" id="dev-table">
      <thead>
        <tr>
          <th>Markė</th>
          <th>Modelis</th>
          <th>Registracijos numeris</th>
          <th>Savininkai</th>
          <th>Redaguoti<th>
          <th>Istrinti<th>
        </tr>
      </thead>

          
@foreach($cars as $key => $carItem)
    <div>
        <tr>
        <td>
        {{ $carItem->brand }}
        </td>
        <td>
        {{ $carItem->model }}
        </td>
        <td>
        {{ $carItem->reg_number}}
        </td>
        <td>
       <a href="{{ route('owners.index', $carItem->id) }}">Rodyti mašinos savininką>></a>
       </td>

       <td>
        <a class="btn btn-info" href="{{ route('masinos.edit', $carItem->id) }}">Redaguoti</a>
       </td>
       @if($key !=0 && $key !=1 && $key !=2)
       <td>
            <form action="{{ route('masinos.destroy', $carItem->id) }}"
            method="POST">
            {{ csrf_field() }}
            <input class="btn btn-danger" type="submit" value="X">
            </form>
       @endif     
       </td>
       </tr>
    </div>     
   @endforeach
   </table>
   @if(Auth::user())
   <a class="btn btn-info" href="{{ route('masinos.create') }}">Pridėti naują mašiną</a>
   @endif
   <hr>

   <a href="{{ route('owners.index', $carItem->id) }}">|| Eiti į visų savininkų sarašą ||</a>

   <h3>Išviso mašinų: {{ $carsCount }}</h3>

</div>
@endsection