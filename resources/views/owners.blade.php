@extends("layouts.app")

@section('content')
<div class="container">

    <div class="panel-heading">
        <h3 class="panel-title">Savininkai</h3>
    </div>


   <table class="table table-hover" id="dev-table">
        <thead>
            <tr>
                <th>Vardas</th>
                <th>Pavardė</th>
                <th>Savininkas nuo</th>
                <th>Savininkas iki</th>
                <th>Savininko mašina</th>
                <th>Turima mašina</th>
            </tr>
        </thead>

@foreach($owners as $savininkas)
<div>
        <tr>
        <td>
        {{ $savininkas->name }}
        </td>
        <td>
        {{ $savininkas->surname }}
        </td> 
        <td>
        {{ $savininkas->from_date}}
        </td>
         <td>
       {{ $savininkas->to_date}}
        </td>
         <td>
       {{ $savininkas->car->brand}} {{ $savininkas->car->model}}
        </td>

        <td>
        <a href="{{ route('masinos.index') }}"><< Grįžti į visas mašinas</a>
        </td>
        </tr>   
    </div>  
@endforeach
</table>
    {{--komentarai rasomi blade faile taip --}}
    <a href="{{ route('masinos.index') }}">|| Eiti į visų mašinų sarašą ||</a>

</div>
@endsection