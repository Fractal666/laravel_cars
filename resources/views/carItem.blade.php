@extends('layouts.app')

@section('content')
<div class="container">

             @foreach($savininkas as $owner)

    <h1>Informacija apie {{ $carItem->brand }}   {{ $carItem->model }}  savininką</h1>   

<table class="table table-hover" id="dev-table">
		<thead>
			<tr>
				<th>Vardas</th>
				<th>Pavardė</th>
				<th>Savininkas nuo</th>
                <th>Savininkas iki</th>
			</tr>
		</thead>

        <tr>
        <td>
        {{ $owner->name }}
        </td>
        <td>
        {{ $owner->surname }}
        </td>
        <td>
        {{ $owner->from_date }}  
        </td>
        <td>
        {{ $owner->to_date }}
        </td>
        </tr>
    <!--
    <p> <a href="{{ route('masinos.index', $owner->id) }}">Istrinti masinos savininka</a> </p>
    -->
    @endforeach
    </table>
    <a href="{{ route('masinos.index') }}"><< Grizti i visas masinas</a>

</div>
@endsection
