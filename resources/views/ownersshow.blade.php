@extends('layouts.app')

@section('content')
<div class="container">

   <div class="panel-heading">
		<h3 class="panel-title">Savininkas</h3>
	</div>


   <table class="table table-hover" id="dev-table">
	<thead>
		<tr>
			<th>Vardas</th>
			<th>Pavardė</th>
			<th>Savininkas nuo</th>
            <th>Savininkas iki</th>
            <th>Savininko mašina</th>
            <th>Visų mašinų sąrašas</th>
		</tr>
	</thead>

    @if(!empty($ownerItem))

    <tr>
    <td>
    {{ $ownerItem->name }}
    </td>
    <td>
    {{ $ownerItem->surname }}
    </td> 
    <td>
    {{ $ownerItem->from_date}}
    </td>
        <td>
    {{ $ownerItem->to_date}}
    </td>
        <td>
    {{ $ownerItem->car->brand}} {{ $ownerItem->car->model}}
    </td>

    <td>
    <a href="{{ route('masinos.index') }}"><< Grįžti į visas mašinas</a>
    </td>
    </tr>   


    </table>

</div>

@else
    <p>
    Mašina neturi šeimininkų
    </p>
    <a href="{{ route('masinos.index') }}"><< Grįžti į visas mašinas</a>

    @endif

@endsection